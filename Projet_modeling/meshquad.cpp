#include "meshquad.h"
#include "matrices.h"

#include <QtDebug>

MeshQuad::MeshQuad():
	m_nb_ind_edges(0)
{

}


void MeshQuad::gl_init()
{
	m_shader_flat = new ShaderProgramFlat();
	m_shader_color = new ShaderProgramColor();

	//VBO
	glGenBuffers(1, &m_vbo);

	//VAO
	glGenVertexArrays(1, &m_vao);
	glBindVertexArray(m_vao);
	glBindBuffer(GL_ARRAY_BUFFER, m_vbo);
	glEnableVertexAttribArray(m_shader_flat->idOfVertexAttribute);
	glVertexAttribPointer(m_shader_flat->idOfVertexAttribute, 3, GL_FLOAT, GL_FALSE, 0, 0);
	glBindVertexArray(0);

	glGenVertexArrays(1, &m_vao2);
	glBindVertexArray(m_vao2);
	glBindBuffer(GL_ARRAY_BUFFER, m_vbo);
	glEnableVertexAttribArray(m_shader_color->idOfVertexAttribute);
	glVertexAttribPointer(m_shader_color->idOfVertexAttribute, 3, GL_FLOAT, GL_FALSE, 0, 0);
	glBindVertexArray(0);


	//EBO indices
	glGenBuffers(1, &m_ebo);
	glGenBuffers(1, &m_ebo2);
}

void MeshQuad::gl_update()
{
	//VBO
	glBindBuffer(GL_ARRAY_BUFFER, m_vbo);
	glBufferData(GL_ARRAY_BUFFER, 3 * m_points.size() * sizeof(GLfloat), &(m_points[0][0]), GL_STATIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, 0);


	std::vector<int> tri_indices;
	convert_quads_to_tris(m_quad_indices,tri_indices);

	//EBO indices
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_ebo);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER,tri_indices.size() * sizeof(int), &(tri_indices[0]), GL_STATIC_DRAW);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);


	std::vector<int> edge_indices;
	convert_quads_to_edges(m_quad_indices,edge_indices);
	m_nb_ind_edges = edge_indices.size();

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_ebo2);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER,m_nb_ind_edges * sizeof(int), &(edge_indices[0]), GL_STATIC_DRAW);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
}



void MeshQuad::set_matrices(const Mat4& view, const Mat4& projection)
{
	viewMatrix = view;
	projectionMatrix = projection;
}

void MeshQuad::draw(const Vec3& color)
{

	glEnable(GL_POLYGON_OFFSET_FILL);
	glPolygonOffset(1.0f, 1.0f);

	m_shader_flat->startUseProgram();
	m_shader_flat->sendViewMatrix(viewMatrix);
	m_shader_flat->sendProjectionMatrix(projectionMatrix);
	glUniform3fv(m_shader_flat->idOfColorUniform, 1, glm::value_ptr(color));
	glBindVertexArray(m_vao);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,m_ebo);
	glDrawElements(GL_TRIANGLES, 3*m_quad_indices.size()/2,GL_UNSIGNED_INT,0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,0);
	glBindVertexArray(0);
	m_shader_flat->stopUseProgram();

	glDisable(GL_POLYGON_OFFSET_FILL);

	m_shader_color->startUseProgram();
	m_shader_color->sendViewMatrix(viewMatrix);
	m_shader_color->sendProjectionMatrix(projectionMatrix);
	glUniform3f(m_shader_color->idOfColorUniform, 0.0f,0.0f,0.0f);
	glBindVertexArray(m_vao2);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,m_ebo2);
	glDrawElements(GL_LINES, m_nb_ind_edges,GL_UNSIGNED_INT,0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,0);
	glBindVertexArray(0);
	m_shader_color->stopUseProgram();
}

void MeshQuad::clear()
{
	m_points.clear();
	m_quad_indices.clear();
}

int MeshQuad::add_vertex(const Vec3& P)
{
    m_points.push_back(P);
    return m_points.size()-1;
}




//    4-------------3
//    |             |
//    1-------------2
void MeshQuad::add_quad(int i1, int i2, int i3, int i4)
{
    m_quad_indices.push_back(i1);
    m_quad_indices.push_back(i2);
    m_quad_indices.push_back(i3);
    m_quad_indices.push_back(i4);

}

void MeshQuad::convert_quads_to_tris(const std::vector<int>& quads, std::vector<int>& tris)
{
    tris.clear();
    tris.reserve(3*quads.size()/2); // 1 quad = 4 indices -> 2 tris = 6 indices d'ou ce calcul (attention division entiere)

    auto add_tri = [&] (int t1, int t2, int t3) -> void
    {
        tris.push_back(t1);
        tris.push_back(t2);
        tris.push_back(t3);
    };
    // Pour chaque quad on genere 2 triangles
    // Attention a respecter l'orientation des triangles
    int i1, i2, i3, i4;
    for(size_t i = 0; i < quads.size(); i+= 4){
        i1 = quads[i];
        i2 = quads[i+1];
        i3 = quads[i+2];
        i4 = quads[i+3];

        add_tri(i1,i2,i3);
        add_tri(i1,i3,i4);
    }
}

void MeshQuad::convert_quads_to_edges(const std::vector<int>& quads, std::vector<int>& edges)
{
    edges.clear();
    edges.reserve(quads.size()); // ( *2 /2 !)

    // Pour chaque quad on genere 4 aretes, 1 arete = 2 indices.
    // Mais chaque arete est commune a 2 quads voisins !
    // Comment n'avoir qu'une seule fois chaque arete ?

    // Le meilleur moyen de s'assurer qu'il n'y est pas de double est de verifier a chaque ajout

    // Correction: Prendre les edges avec A > B ou l'inverse

    int i1, i2, i3, i4;
    bool addE1, addE2, addE3, addE4;
    size_t j;
    for(size_t i = 0; i < quads.size(); i+= 4){
        addE1 = addE2 = addE3 = addE4 = true;

        i1 = quads[i];
        i2 = quads[i+1];
        i3 = quads[i+2];
        i4 = quads[i+3];

        for(j = 0; j < edges.size(); j+= 2){
            int ej = edges.at(j), ejp = edges.at(j+1);

            if(ej == i1 && ejp == i2) addE1 = false;
            else if(ej == i2 && ejp == i3) addE2 = false;
            else if(ej == i3 && ejp == i4) addE3 = false;
            else if(ej == i4 && ejp == i1) addE4 = false;
        }

        if(addE1)
        {
          edges.push_back(i1);
          edges.push_back(i2);
        }

        if(addE2)
        {
          edges.push_back(i2);
          edges.push_back(i3);
        }

        if(addE3)
        {
          edges.push_back(i3);
          edges.push_back(i4);
        }

        if(addE4)
        {
          edges.push_back(i4);
          edges.push_back(i1);
        }
    }
}


void MeshQuad::create_cube()
{
	clear();
    // ajouter 8 sommets (-1 +1)

    // Face avant
    int s1 = add_vertex(Vec3(-0.5, -0.5, 0.5)); // bas gauche
    int s2 = add_vertex(Vec3(0.5, -0.5, 0.5));  // bas droite
    int s3 = add_vertex(Vec3(-0.5, 0.5, 0.5));  // haut gauche
    int s4 = add_vertex(Vec3(0.5, 0.5, 0.5));   // haut droite

    // Face arriere
    int s5 = add_vertex(Vec3(-0.5, -0.5, -0.5));  // bas gauche
    int s6 = add_vertex(Vec3(0.5, -0.5, -0.5));   // bas droite
    int s7 = add_vertex(Vec3(-0.5, 0.5, -0.5));    // haut gauche
    int s8 = add_vertex(Vec3(0.5, 0.5, -0.5));  // haut droite

    // ajouter 6 faces (sens trigo)

    // avant
    add_quad(s1, s2, s4, s3);
    // droite
    add_quad(s2, s6, s8, s4);
    // gauche
    add_quad(s5, s1, s3, s7);
    // fond
    add_quad(s6, s5, s7, s8);
    // haut
    add_quad(s7, s3, s4, s8);
    // bas
    add_quad(s6, s2, s1, s5);

	gl_update();
}

void MeshQuad::create_etoile(){
    clear();

    create_cube();

    int iteration = 1;
    while(iteration < 15){
        for(size_t it = 0; it < 6; it++){
            extrude_quad(it);
            decale_quad(it, -0.5);
            tourne_quad(it, 7);
            shrink_quad(it, -0.1);
        }

        ++iteration;
    }

    gl_update();

}

void MeshQuad::create_spirale(){
    clear();

    create_cube();

    int iteration = 0;
    int size = 200;

    double incMonter = 10;
    double incTourner = 5;

    while(iteration < size){
        extrude_quad(0);

        shrink_quad(0, -0.020);


        // Monter
        tourne_quad(iteration * 4 +7, incMonter);
        tourne_quad(iteration * 4 + 6, -incMonter);

        // Tourner
        tourne_quad(iteration * 4 + 9, incTourner);
        tourne_quad(iteration * 4 + 8, -incTourner);


        ++iteration;

    }



    gl_update();
}

void MeshQuad::create_serpent(){
    clear();

    create_cube();

    int iteration = 0;

    while(iteration < 200){
        extrude_quad(0);

        // Monter
        tourne_quad(iteration * 4 +7, iteration * 0.05);
        tourne_quad(iteration * 4 + 6, iteration * -0.05);

        // tourner
        tourne_quad(iteration * 4 + 9, iteration * -0.1);
        tourne_quad(iteration * 4 + 8, iteration * 0.1);


        ++iteration;

    }



    gl_update();
}

Vec3 MeshQuad::normal_of_quad(const Vec3& A, const Vec3& B, const Vec3& C, const Vec3& D)
{
	// Attention a l'ordre des points !
	// le produit vectoriel n'est pas commutatif U ^ V = - V ^ U
    // ne pas oublier de normaliser le resultat.

    // d------------c
    // |            |
    // a------------b

    // -> le produit en crois donne le vecteur à 90deg des deux vecteurs
    // et donc la normal du quad
    // Sachant que les 4 points sont sur le meme plan
    Vec3 dot = glm::cross(A - D, C - D);
    return glm::normalize(dot);
}

float MeshQuad::area_of_quad(const Vec3& A, const Vec3& B, const Vec3& C, const Vec3& D)
{
    // aire du quad = aire tri + aire tri

	// aire du tri = 1/2 aire parallelogramme

    // aire parallelogramme: cf produit vectoriel
    // -> base * hauteur
    // base : AB
    // hauteur : AC * angle (CAB)
    // angle CAB :
    //    cos angle = (AB . AC) / (len(AB) * len(AC))
    //      angle = arccossine(cos angle)
    // Je n'ai pas reussis à utiliser la methode de l'angle pour calculer l'aire


    float result;
    Vec3 AB = B-A, AC = C-A, CD = D-C, CB = B-C;

    return glm::length(glm::cross(AB, AC)) / 2 + glm::length(glm::cross(CB, CD)) / 2;
}


bool MeshQuad::is_points_in_quad(const Vec3& P, const Vec3& A, const Vec3& B, const Vec3& C, const Vec3& D)
{
	// On sait que P est dans le plan du quad.

	// P est-il au dessus des 4 plans contenant chacun la normale au quad et une arete AB/BC/CD/DA ?
    // si oui il est dans le quad

    // Un plan : a.x + b.y + c.z + d = 0
    // (a,b,c) la normale du plan : la normale du quad
    // x, y, z : les cords des 4 points

#ifdef QT_DEBUG
    qDebug() << "** Is P("
             <<P.x << "," << P.y << ", " << P.z << ") in ABCD ? "
            << "\n**   A(" << A.x << "," << A.y << ", " << A.z
            << "\n**   B(" << B.x << "," << B.y << ", " << B.z
            << "\n**   C(" << C.x << "," << C.y << ", " << C.z
            << "\n**   D(" << D.x << "," << D.y << ", " << D.z;
#endif
    Vec3 N = normal_of_quad(A, B, C, D);

    // Pour chaque plan
    // On cherche le vecteur orthogonal à la normale et au vecteur d'une arrête
    // Ce vecteur donne l'équation du plan
    // On peut ensuite vérifier si le point est au dessus du plan
    // Lien utile: http://tutorial.math.lamar.edu/Classes/CalcIII/EqnsOfPlanes.aspx

    Vec3 NP; // La normal du plan orthogonal a la normal et une arrete
    float dir; // Resultat de lequation du plan avec le point P, si > O, alors au dessus du plan

    // P1 : le premier point de l'arrete
    // P2 : le deuxieme
    auto is_point_above_plane = [&] (Vec3 P1, Vec3 P2) -> bool
    {
      NP = glm::cross(N, P2 - P1);
      dir = glm::dot(NP, P - P1);
      return dir > 0;
    };

    // Plane 1
    return is_point_above_plane(A, B)
            && is_point_above_plane(B, C)
            && is_point_above_plane(C, D)
            && is_point_above_plane(D, A);

}

// D----------C
// |          |
// A----------B
bool MeshQuad::intersect_ray_quad(const Vec3& P, const Vec3& Dir, int q, Vec3& inter)
{
#ifdef QT_DEBUG
    qDebug() << "II Is P("
             << P.x << "," << P.y << "," << P.z << ")->("
             << Dir.x << "," << Dir.y << "," << Dir.z
             << ") intersecting quad number " << q << " ?";
#endif
	// recuperation des indices de points
    // recuperation des points
    Vec3 A = m_points[m_quad_indices[q*4]];
    Vec3 B = m_points[m_quad_indices[q*4 +1]];
    Vec3 C = m_points[m_quad_indices[q*4 +2]];
    Vec3 D = m_points[m_quad_indices[q*4 +3]];
#ifdef QT_DEBUG
    qDebug() << "II A: "  << A.x << ", " << A.y << ", " << A.z;
    qDebug() << "II B: "  << B.x << ", " << B.y << ", " << B.z;
    qDebug() << "II C: "  << C.x << ", " << C.y << ", " << C.z;
    qDebug() << "II D: "  << D.x << ", " << D.y << ", " << D.z;
#endif

    // calcul de l'equation du plan (N+d)
    // a⋅x+ b⋅y + c⋅z  + d
    Vec3 norm = normal_of_quad(A, B, C, D);
    float d = -(norm.x * A.x + norm.y * A.y + norm.z * A.z);

	// calcul de l'intersection rayon plan
    // I = P + alpha*Dir est dans le plan => calcul de alpha
    float denom = glm::dot(norm, Dir);
    if(abs(denom) > 0){ // Eviter la division par zero
        float alpha = glm::dot(A - P, norm) / denom;
        if(alpha >= 0){
          // alpha => calcul de I
            Vec3 I = P + alpha * Dir;
            // I dans le quad ?
            if(is_points_in_quad(I, A, B, C, D)){
                inter = I;
#ifdef QT_DEBUG
                qDebug() << "II Yes";
#endif
                return true;
            }

        }

    }
#ifdef QT_DEBUG
    qDebug() << "II No.";
#endif
    return false;


}


int MeshQuad::intersected_visible(const Vec3& P, const Vec3& Dir)
{
	// on parcours tous les quads
	// on teste si il y a intersection avec le rayon
	// on garde le plus proche (de P)

    int inter = -1;
    float minDist;
    Vec3 I;

    for(int i = 0; i < m_quad_indices.size()/4; i++){
       if(intersect_ray_quad(P, Dir, i, I)){
           if(inter == -1 || glm::distance(P, I) < minDist){
               minDist = glm::distance(P, I);
               inter = i;
           }
       }
    }

	return inter;
}

Mat4 MeshQuad::local_frame(int q)
{
    // Repere locale = Matrice de transfo avec
    // les trois premieres colones: X,Y,Z locaux
    // la derniere colonne l'origine du repere

    // ici Z = N et X = AB
    // Origine le centre de la face
    // longueur des axes : [AB]/2

    // recuperation des indices de points
    // recuperation des points
    Vec3 A = m_points[m_quad_indices[q*4]];
    Vec3 B = m_points[m_quad_indices[q*4 +1]];
    Vec3 C = m_points[m_quad_indices[q*4 +2]];
    Vec3 D = m_points[m_quad_indices[q*4 +3]];

    // calcul de Z:N puis de X:arete on en deduit Y
    Vec3 Z = normal_of_quad(A, B, C, D);
    Vec3 X = glm::normalize(Vec3(B - A));
    Vec3 Y = glm::normalize(glm::cross(Z, X));

    // calcul du centre
    Vec3 center((A.x + B.x + C.x + D.x) / 4.0,
                (A.y + B.y + C.y + D.y) / 4.0,
                (A.z + B.z + C.z + D.z) / 4.0);

    // calcul de la taille
    float taille = glm::length(B - A) / 2.0;

    // calcul de la matrice
    Mat4 xyz;

    xyz[0][0] = X.x;
    xyz[0][1] = X.y;
    xyz[0][2] = X.z;
    xyz[1][0] = Y.x;
    xyz[1][1] = Y.y;
    xyz[1][2] = Y.z;
    xyz[2][0] = Z.x;
    xyz[2][1] = Z.y;
    xyz[2][2] = Z.z;
    xyz[3][0] = center.x;
    xyz[3][1] = center.y;
    xyz[3][2] = center.z;

    Mat4 res = xyz * scale(taille, taille, taille);



    return res;
}

void MeshQuad::extrude_quad(int q)
{
    // recuperation des indices de points
    int iA = m_quad_indices[q*4];
    int iB = m_quad_indices[q*4 + 1];
    int iC = m_quad_indices[q*4 + 2];
    int iD = m_quad_indices[q*4 + 3];
    Vec3 A = m_points[iA];
    Vec3 B = m_points[iB];
    Vec3 C = m_points[iC];
    Vec3 D = m_points[iD];

	// recuperation des points

    // calcul de la normale
    Vec3 norm = normal_of_quad(A, B, C, D);

    // calcul de la hauteur
    float h = sqrt( area_of_quad(A, B, C, D));

    // calcul et ajout des 4 nouveaux points
    Vec3 A2 = A + h * norm;
    Vec3 B2 = B + h * norm;
    Vec3 C2 = C + h * norm;
    Vec3 D2 = D + h * norm;
    int iA2 = add_vertex(A2);
    int iB2 = add_vertex(B2);
    int iC2 = add_vertex(C2);
    int iD2 = add_vertex(D2);

    // on remplace le quad initial par le quad du dessus
    m_quad_indices.reserve(4);
    m_quad_indices[q*4] = iA2;
    m_quad_indices[q*4 + 1] = iB2;
    m_quad_indices[q*4 + 2] = iC2;
    m_quad_indices[q*4 + 3] = iD2;

    // on ajoute les 4 quads des cotes
    add_quad(iA, iA2, iD2, iD);
    add_quad(iB2, iB, iC, iC2);
    add_quad(iA, iB, iB2, iA2);
    add_quad(iD, iD2, iC2, iC);



	gl_update();
}


void MeshQuad::decale_quad(int q, float d)
{
	// recuperation des indices de points

	// recuperation des (references de) points
    int iA = m_quad_indices[q*4];
    int iB = m_quad_indices[q*4 + 1];
    int iC = m_quad_indices[q*4 + 2];
    int iD = m_quad_indices[q*4 + 3];
    Vec3 &A = m_points[iA];
    Vec3 &B = m_points[iB];
    Vec3 &C = m_points[iC];
    Vec3 &D = m_points[iD];

    // calcul de la normale
    Vec3 norm = normal_of_quad(A, B, C, D);
    float h = d * sqrt( area_of_quad(A, B, C, D));


	// modification des points
    A = A +  h * norm;
    B = B +  h * norm;
    C = C +  h * norm;
    D = D +  h * norm;

	gl_update();
}


void MeshQuad::shrink_quad(int q, float s)
{
  // ici  pas besoin de passer par une matrice
    int iA = m_quad_indices[q*4];
    int iB = m_quad_indices[q*4 + 1];
    int iC = m_quad_indices[q*4 + 2];
    int iD = m_quad_indices[q*4 + 3];
    Vec3 &A = m_points[iA];
    Vec3 &B = m_points[iB];
    Vec3 &C = m_points[iC];
    Vec3 &D = m_points[iD];

    Vec3 center((A.x + B.x + C.x + D.x) / 4.0,
                (A.y + B.y + C.y + D.y) / 4.0,
                (A.z + B.z + C.z + D.z) / 4.0);



    A = A + s * (A - center);
    B = B + s * (B - center);
    C = C + s * (C - center);
    D = D + s * (D - center);
    gl_update();
}


void MeshQuad::tourne_quad(int q, float a)
{
	// recuperation des indices de points

    int iA = m_quad_indices[q*4];
    int iB = m_quad_indices[q*4 + 1];
    int iC = m_quad_indices[q*4 + 2];
    int iD = m_quad_indices[q*4 + 3];
    // recuperation des (references de) points
    Vec3 &A = m_points[iA];
    Vec3 &B = m_points[iB];
    Vec3 &C = m_points[iC];
    Vec3 &D = m_points[iD];

    // generation de la matrice de transfo:
    // tourne autour du Z de la local frame
    // indice utilisation de glm::inverse()
    // 1. retour à l'origine
    // 2. rotation
    // 3. retour à l'origine
    // Mat = 3 * 2 * 1
    Mat4 local = local_frame(q);
    Mat4 trans = local * rotateZ(a) * glm::inverse(local);


    // Application au 4 points du quad
    A = Vec3(trans * Vec4(A, 1));
    B = Vec3(trans * Vec4(B, 1));
    C = Vec3(trans * Vec4(C, 1));
    D = Vec3(trans * Vec4(D, 1));



	gl_update();
}

void MeshQuad::apply_mat_to_point(int p, Mat4 transf){
    if(p < 0 || p > m_points.size()) return;
    m_points[p] = Vec3(transf * Vec4(m_points[p], 1));
}

void MeshQuad::apply_mat_to_quad_points(int q, Mat4 transf){
    apply_mat_to_point(m_quad_indices[q * 4], transf);
    apply_mat_to_point(m_quad_indices[q * 4 + 1], transf);
    apply_mat_to_point(m_quad_indices[q * 4 + 1], transf);
    apply_mat_to_point(m_quad_indices[q * 4 + 1], transf);
}

